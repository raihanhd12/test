<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        $str = Str::random(100);
        \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'role' => 'admin',
            'password' => 'admin123',
            'verify_key' => $str,
        ]);
        \App\Models\User::factory()->create([
            'name' => 'User1',
            'email' => 'user@gmail.com',
            'role' => 'user',
            'password' => 'user123',
            'verify_key' => $str,
        ]);
    }
}
