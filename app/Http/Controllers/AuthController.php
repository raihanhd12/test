<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\AuthMail;
use illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;

class AuthController extends Controller
{
    function index()
    {
        return view('auth/login');
    }
    function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',

        ], [
            'email.required' => 'Email must be filled in',
            'password.required' => 'Password must be filled in',
        ]);

        $infologin = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($infologin)) {
            if (Auth::user()->email_verified_at != null) {
                if (Auth::user()->role === 'admin') {
                    return redirect()->route('admin')->with('success', 'Admin logged in');
                } else if (Auth::user()->role === 'user') {
                    return redirect()->route('user')->with('success', 'Successfully logged in');
                }
            } else {
                Auth::logout();
                return redirect()->route('auth')->withErrors('Your account is not active. Please Verify first');
            }
        } else {
            return redirect()->route('auth')->withErrors('Email or Password incorrect');
        }
    }

    function logout(Request $request): RedirectResponse
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    function create()
    {
        return view('auth/register');
    }

    function register(Request $request)
    {
        $str = Str::random(100);
        $request->validate([
            'name' => 'required|min:2',
            'email' => 'required|unique:users|email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',

        ], [
            'name.required' => 'Name is required',
            'name.min' => 'Name must be at least 2 characters',
            'email.required' => 'Email is required',
            'email.unique' => 'Email has been registered',
            'password.required' => 'Password is required',
            'password.min' => 'Password must be at least 6 characters',
            'password_confirmation.required' => 'The password field confirmation does not match'

        ]);

        $inforegister = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'verify_key' => $str
        ];

        User::create($inforegister);

        $details = [
            'name' => $inforegister['name'],
            'role' => 'user',
            'datetime' => date('Y-m-d H:i:s'),
            'website' => 'Multi - Social Media Channel Integration',
            'url' => 'http://' . request()->getHttpHost() . "/" . "verify/" . $inforegister['verify_key'],
        ];

        Mail::to($inforegister['email'])->send(new AuthMail($details));

        return redirect()->route('auth')->with('success', 'Verification link has been sent to your email, check your email to verify');
    }
    
    function verify($verify_key)
    {
        $keyCheck = User::select('verify_key')->where('verify_key', $verify_key)->exists();

        if ($keyCheck) {
            $user = User::where('verify_key', $verify_key)->update(['email_verified_at' => date('Y-m-d H:i:s')]);

            return redirect()->route('auth')->with('success', 'Successful verification. your account is already active.');
        } else {
            return redirect()->route('auth')->withErrors('Invalid keys. make sure you have registered')->withInput();
        }
    }
}
