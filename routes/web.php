<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::view('/', 'home/index');
    Route::get('/session', [AuthController::class, 'index'])->name('auth');
    Route::post('/session', [AuthController::class, 'login']);
    Route::get('/reg', [AuthController::class, 'create'])->name('registration');
    Route::post('/reg', [AuthController::class, 'register']);
    Route::get('/verify/{verify_key}', [AuthController::class, 'verify']);
});

Route::middleware(['auth'])->group(function () {
    Route::redirect('/home', '/user');
    Route::get('/admin', [AdminController::class, 'index'])->name('admin')->middleware('roleUser:admin');
    Route::get('/user', [UserController::class, 'index'])->name('user')->middleware('roleUser:user');
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});
